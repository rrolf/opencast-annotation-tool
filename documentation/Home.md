# Documentation
This wiki represents the main documentation for the AAA annotations project.

For questions or more informations, visit IRC channel _aaa-annotations_ on _irc.freenode.net_.

## [News](News.md)
The latest news about the annotations tool.

## [Rest API](Rest-API.md)
How should the Rest API be used and implemented.

## [Player adapter API](Player-adapter-API.md)
Documentation of the player adapter interface, making the link between the annotations library and the video player.

## [Javascript API](http://entwinemedia.github.com/annotations/docs/index.html)
The documentation of the javascript API from the annotations tool.

## [Build script](build-script.md)
Small Documentation for the ANT build script provided with the tool.

## [Supported Browsers](Supported-browsers.md)
List of supported browsers.

## [Roadmap](Roadmap.md)
List of goal for the upcoming version.

## [Test with preset](Test-the-tool-with-an-annotations-preset.md)
Test the tool with a large set of annotations.

## [Demo](http://entwinemedia.github.com/annotations)
Demo of the annotation library hosted on github. Note that the demo stores annotations in the browser's local storage and not on a server backend. Therefore you will currently not see annotations made by others.
